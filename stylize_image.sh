
content_dir="../../InProgress/grenoble_chartreuse2" #$(dirname "$content_image")
content_filename="IMG_2904.jpeg"  #$(basename "$content_image")
style_dir="../basePatterns"  #$(dirname "$style_image" )
style_filename="impasto_vangogh_sn_hires.jpg" #$(basename "$style_image")
special_init_filename="vangogh_sn_init_2.png"
init_img_type="special" #"special"
seed=5
make_array=0 
make_mirror=0
max_size=2000 #1200 # 1350 max dimension on gpu? can go to 2000 on CPU only if we want

print_iters=100
max_iterations=1000 # the more you iterate, the closer you get to final image? # content seems to come through during late iteratioins. also subtle details emerge late. sometimes gray areas fade into subtle details during late iters. grid artifacts also appear to lighten in late iters

style_weight=2500 # as you go up in resolution, the style weight decreases and content weight increases
content_weight=0.3 #5 0.025  # higher for making final image closer to original
tv_weight=0.3 # allows us to get rid of some grid artifacts
learning_rate=0.6 # extremely important. lowering will remove gray areas. increasing it will increase brush stroke a bit? increasing takes alot longer to get to stable state. Seems better to use larger value for low res init_img development, then decrease alot for keeping large brush strokes when moving up to higher res. Adujusting in neighborhood can have large effect on color pallette temperature etc. 

optimizer='adam'
device='/cpu:0'

#style_layers=('conv1_1' 'relu1_1' 'pool1' 'conv2_1' 'relu2_1' 'pool2' 'conv3_1' 'relu3_1' 'pool3' 'conv4_1' 'relu4_1' 'pool4' 'conv5_1' 'relu5_1' 'pool5') #('conv1_1' 'conv2_1' 'conv3_1' 'conv4_1' 'conv5_1')
style_layers=('conv1_2' 'conv2_1' 'conv3_1' 'relu3_1' 'conv4_1' 'pool4' 'conv5_1' 'relu5_1')
style_layer_weights=(0.2 0.2 0.2 0.2 0.2) # must match lenght of style layers!!# 0.2 0.2) # must match lenght of style layers!!
content_layers=('conv1_1' 'conv2_1')
content_layer_weights=(0.5 0.5)

echo "Rendering stylized image. This may take a while..."
python3 neural_style.py \
--verbose \
--original_colors \
--content_img "${content_filename}" \
--content_img_dir "${content_dir}" \
--style_imgs "${style_filename}" \
--style_imgs_dir "${style_dir}" \
--img_output_dir "${content_dir}" \
--img_name "${style_filename}" \
--max_iterations "${max_iterations}" \
--max_size "${max_size}" \
--tv_weight "${tv_weight}" \
--optimizer "${optimizer}" \
--device "${device}" \
--style_weight "${style_weight}" \
--content_weight "${content_weight}" \
--special_init "${special_init_filename}" \
--init_img_type "${init_img_type}" \
--seed "${seed}" \
--make_array "${make_array}" \
--optimizer "${optimizer}" \
--make_mirror "${make_mirror}" \
--print_iterations "${print_iters}" \
--learning_rate "${learning_rate}" \
--style_layer_weights "${style_layer_weights[@]}" \
--content_layer_weights "${content_layer_weights[@]}" \
--style_layers "${style_layers[@]}" \
--content_layers "${content_layers[@]}" \



